<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Article;
class ArticleController extends Controller
   {
 
     public function index()
    {
        $articles = Article::paginate(5);
        return view("laravel")->with("articles", $articles);
    }

    public function store(Request $request)
    {
            $article = new Article; 
            $article->title = $request->input('title'); 
            $article->content = $request->input('content'); 
            $article->save();

          echo "Success";
        }
    public function create()
    {
        return view('paywithpaypal');
    }
    public function show($id)
    {
        $article =  Article::find($id);
        return view("show")->with("article", $article);
    }
    public function edit($id)
    {
        $article =  Article::find($id);
        return view("editform")->with("article", $article);
     }
    public function update(Request $request, $id)
    {
        $article =  Article::find($id);
        $article->title = $request->title; 
        $article->content = $request->content; 
        $article->save();
        echo "Success"; 

    }

    
    public function destroy($id)
    {
        Article::destroy($id);
        echo"Success";
    }
}
