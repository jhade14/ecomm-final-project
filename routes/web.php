<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('laravel');
});
Route::get('/addform', function () {
    return view('addform');
});
Route::get('/editform', function () {
    return view('editform');
});
Route::get('/show', function () {
    return view('show');
});
Route::get('/blog', function () {
    return view('blog');
});
Route::get('/cart', function () {
    return view('cart');
});
Route::get('/categories', function () {
    return view('categories');
});
Route::get('/product', function () {
    return view('product');
});
Route::get('/checkout', function () {
    return view('checkout');
});
Route::get('/index', function () {
    return view('index');
});
Route::resource('laravel', 'ArticleController');

Route::post('paypal', 'PaymentController@payWithpaypal');

Route::get('status', 'PaymentController@getPaymentStatus');
    