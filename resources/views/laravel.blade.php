<!doctype html>
<html>
    <head>
    <style >
        table{
            font-family: arial, sans-serif;
            border-collapse: collapse;;
            width: 100%;
        }
        td, th{
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;

        }
.pagination li{
    color: black;
    float: left;
    padding: 8px 10px;
    transition: background-color .3s;
    list-style-type: none;
}
.pagination li:hover{
    background: red;
}
    </style>
    </head>
    <body>
        <table>
        <tr>
            <th>id</th>
            <th>Name</th>
            <th>Amount</th>
            <th>Action</th>
        </tr>
        <a href="/laravel/create">add new</a>
        @foreach($articles as $article)
        <tr>
            <td>{{$article->id}}</td>
            <td>{{$article->title}}</td>
            <td>{{$article->content}}</td>

            <td><a href= "/laravel/{{$article->id}}">View</a> | <a href="/laravel/{{$article->id}}/edit">Edit</a>
           
            <form action="laravel/{{$article->id}}" method="POST"> 
                @csrf
                @method("DELETE")

                <input type="submit" name="submit" value="DELETE">
                
            </form>
            </td>
        </tr>
        @endforeach

        </table>
        {{$articles->links()}}
    </body>
</html>
